<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{

    public function index()
    {
        abort_if(Gate::denies('role_access'),
        403,
        'you are not able to access this resource');

        $roles=Role::all();
        return view('admin.user_management.role.index', compact('roles'));
    }

    public function create()
    {
        abort_if(Gate::denies('role_create'),
        403,
        'you are not able to access this resource');

        $permissions=Permission::all();
        return view('admin.user_management.role.create',compact('permissions'));
    }

    public function store(StoreRoleRequest $request)
    {
        Role::create($request->validated());

        toast('Role Added Successfully', 'success');
        return back();
    }


    public function show(Role $role)
    {
        //
    }

    public function edit(Role $role)
    {
        abort_if(Gate::denies('role_create'),
            403,
            'you are not able to access this resource');
        $permissions=Permission::all();
        return view('admin.user_management.role.edit',compact('role','permissions'));
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        abort_if(Gate::denies('role_edit'),
        403,
        'you are not able to access this resource');

        $role->update($request->validated());

        toast('Role Edited Successfully', 'success');
        return redirect(route('admin.userManagement.role.index'));
    }

    public function destroy(Role $role)
    {
        $role->delete();

        toast('Role Deleted Successfully','success');
        return back();
    }
}
