<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{

    public function index()
    {
        abort_if(Gate::denies('user_access'),
        403,
        'you are not able to access this resource');

        $users=User::with('role')->whereNot('id', auth()->id())->get();
        return view('admin.user_management.user.index', compact('users'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'),
        403,
        'you are not able to access this resource');

        $roles=Role::all();

        return view('admin.user_management.user.create', compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        abort_if(Gate::denies('user_create'),
            403,
            'you are not able to access this resource');
        User::create($request->validated());

        toast('Users Added Successfully', 'success');
        return back();
    }


    public function show(User $user)
    {
        //
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('user_edit'),
            403,
            'you are not able to access this resource');
        $roles=Role::all();
        return view('admin.user_management.user.edit', compact('user','roles'));
    }

    public function update(Request $request, User $user)
    {
        abort_if(Gate::denies('user_edit'),
            403,
            'you are not able to access this resource');

        $user->update($request->validated());

        toast('User Updated Successfully', 'success');
        return redirect(route('admin.userManagement.user.index'));
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'),
            403,
            'you are not able to access this resource');

        $user->delete();

        toast('User Deleted Successfully', 'danger');
        return back();
    }
}
