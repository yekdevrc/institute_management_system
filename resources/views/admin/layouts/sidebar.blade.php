<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item nav-category">User Management</li>
        <li class="nav-item">
            <a class="nav-link {{ request()->is('admin.userManagement.role.*')?'':'collapsed' }}"
               data-bs-toggle="collapse" href="#auth"
               {{request()->is('admin.userManagement.role.*')? 'aria-expanded=true' : ''}} aria-controls="auth">
                <i class="menu-icon mdi mdi-account-circle-outline"></i>
                <span class="menu-title">User & Role</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                    @can('user_access')
                        <li class="nav-item"><a class="nav-link" href="{{route('admin.userManagement.user.index')}}">
                                User </a></li>
                    @endcan
                    @can('role_access')
                        <li class="nav-item"><a class="nav-link" href="{{route('admin.userManagement.role.index')}}">
                                Role </a></li>
                    @endcan
                </ul>
            </div>
        </li>
    </ul>
</nav>
