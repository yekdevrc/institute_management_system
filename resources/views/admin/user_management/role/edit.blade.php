@extends('admin.layouts.master')
{{--    <div class="row">--}}
{{--        <div class="col-md-12 mb-2">--}}
{{--            <div class="card">--}}
{{--                <div class="col-lg-12 grid-margin stretch-card">--}}
{{--                    <div class="card">--}}
{{--                        <div class="card-body">--}}
{{--                            <div class="d-flex justify-content-between">--}}
{{--                                <h4 class="card-title">Role</h4>--}}
{{--                                @can('role_create')--}}
{{--                                    <a href="{{route('admin.userManagement.role.create')}}" class="btn btn-md btn-rounded btn-outline-info">--}}
{{--                                        <i class="fa fa-plus-circle"></i>  Add New--}}
{{--                                    </a>--}}
{{--                                @endcan--}}
{{--                            </div>--}}
{{--                <div class="card-body">--}}
{{--                    <form action="{{route('admin.userManagement.role.store')}}" method="post">--}}
{{--                        @csrf--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-12 mb-2">--}}
{{--                                <label for="title" class="form-label">Title *</label>--}}
{{--                                <input type="text"--}}
{{--                                       name="title"--}}
{{--                                       class="form-control @error('title') is-invalid @enderror"--}}
{{--                                       id="title"--}}
{{--                                       value="{{old('title')}}"--}}
{{--                                       placeholder="enter title"--}}
{{--                                >--}}
{{--                                @error('title')--}}
{{--                                <div class="invalid-feedback">{{$message}}</div>--}}
{{--                                @enderror--}}
{{--                            </div>--}}

{{--                            <div class="col-md-12 mb-3">--}}
{{--                                <label for="permissions" class="form-label">Permissions *</label>--}}
{{--                                <fieldset>--}}
{{--                                    <div class="row">--}}
{{--                                        @foreach($permissions as $permission)--}}
{{--                                            <div class="col-sm-3">--}}
{{--                                                <div class="form-check form-check-flat form-check-primary">--}}
{{--                                                    <label class="form-check-label">--}}
{{--                                                        <input type="checkbox" name="permissions[]"--}}
{{--                                                               value="{{$permission->id}}" class="form-check-input">--}}
{{--                                                        {{$permission->title}}--}}
{{--                                                    </label>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    </div>--}}
{{--                                </fieldset>--}}
{{--                                @error('permissions')--}}
{{--                                <p class="text-danger">{{$message}}</p>--}}
{{--                                @enderror--}}
{{--                                @error('permissions.*')--}}
{{--                                <p class="text-danger">{{$message}}</p>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button type="submit" class="btn btn-primary">--}}
{{--                            Save--}}
{{--                        </button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


@section('content')
    <div class="row mb-3">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <div>
                    Home/ UserManagement
                </div>
                <div class="active">Role</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title">Role</h4>
                    @can('role_access')
                        <a href="{{route('admin.userManagement.role.index')}}"
                           class="btn btn-md btn-rounded btn-outline-info">
                            <i class="fa fa-plus-circle"></i> Role List
                        </a>
                    @endcan
                </div>
                <form action="{{route('admin.userManagement.role.update',$role)}}" method="post">
                    @csrf
                    @method('put')
                    <div class="row m-1">
                        <label for="title" class="form-label">Title *</label>
                        <input
                            type="text"
                            name="title"
                            id="title"
                            value="{{old('title',$role->title)}}"
                            class="form-control @error('title') is-invalid @enderror"
                            placeholder="Title"
                            aria-label="Username">
                        @error('title')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <label for="permissions" class="form-label">Permissions *</label>
                    <div class="row">
                        @foreach($permissions as $permission)
                            <div class="col-sm-3 mb-1">
                                <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="permissions[]"
                                               value="{{$permission->id}}"
                                               {{in_array($permission->id,$role->permissions->pluck('id')->toArray()) ? 'checked' : ''}}
                                               class="form-check-input">
                                        {{$permission->title}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                        @error('permissions')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                        @error('permissions.*')
                        <div class="invalid-feedback">{{$message}}</div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">
                        Save
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
