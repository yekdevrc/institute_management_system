@extends('admin.layouts.master')

@section('content')
    <div class="row mb-3">
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <div>
                    Home/ UserManagement
                </div>
                <div class="active">User</div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title">User</h4>
                    @can('user_create')
                        <a href="{{route('admin.userManagement.user.create')}}" class="btn btn-md btn-rounded btn-outline-info">
                            <i class="fa fa-plus-circle"></i>  Add New
                        </a>
                    @endcan
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($users as $user)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$user->name}}</td>
                                <td class="d-flex m-1 p-1">
                                    @can('user_edit')
                                        <a href="{{route('admin.userManagement.user.edit', $user)}}"
                                           class="btn btn-xs btn-sm btn-outline-primary">
                                            <i class="fa fa-pencil-square"></i> edit
                                        </a>
                                    @endcan
                                    @can('user_delete')
                                        <form action="{{route('admin.userManagement.user.destroy', $user)}}"
                                              method="post" onsubmit="return confirm('Are you sure to Delete?')">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-xs btn-sm btn-outline-danger show_confirm">
                                                <i class="fa fa-trash"></i> delete
                                            </button>
                                        </form>
                                    @endcan
                                </td>
                                @empty
                                    <td class="text-center" colspan="3">No Data Found</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
