<?php


use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('dashboard', DashboardController::class)->name('dashboard');

Route::prefix('userManagement')->as('userManagement.')->group(function (){
   Route::resource('role', RoleController::class);
   Route::resource('user', UserController::class);
});
